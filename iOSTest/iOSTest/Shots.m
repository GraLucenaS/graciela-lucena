//
//  Shots.m
//  iOSTest
//
//  Created by Graciela Lucena on 9/28/15.
//  Copyright (c) 2015 Graciela Lucena. All rights reserved.
//

#import "Shots.h"
#import "User.h"

@interface Shots () <MTLJSONSerializing>

@property (copy, nonatomic) NSDictionary *userData;

@end

@implementation Shots

#pragma mark - Mantle related methods
+ (NSDictionary *)JSONKeyPathsByPropertyKey {
    return @{
             @"shotId"      : @"id",
             @"shotTittle" : @"title",
             @"shotViews"    : @"views_count",
             @"imageUrl"    : @"image_url",
             @"shotDescription"    : @"description",
             @"userData"    : @"player"
             };
}

- (User *)user
{
    NSError *error;
    User *user = [MTLJSONAdapter modelOfClass:[User class] fromJSONDictionary:_userData error:&error];
    if (error) NSAssert(NO, @"Error creating user model for note");
    
    return user;
}


@end
