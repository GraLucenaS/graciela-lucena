//
//  DetailViewController.h
//  iOSTest
//
//  Created by Graciela Lucena on 9/29/15.
//  Copyright (c) 2015 Graciela Lucena. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "Shots.h"

@interface DetailViewController : UIViewController

@property (weak, nonatomic) IBOutlet UIImageView *shotImage;
@property (weak, nonatomic) IBOutlet UIImageView *userAvatar;
@property (weak, nonatomic) IBOutlet UILabel *userName;
@property (weak, nonatomic) IBOutlet UITextView *shotDescription;
@property (weak, nonatomic) IBOutlet UILabel *viewsCount;
@property (weak, nonatomic) IBOutlet UILabel *shotTittle;

@property (strong, nonatomic) Shots* selectedShot;

@end
