//
//  User.h
//  iOSTest
//
//  Created by Graciela Lucena on 9/28/15.
//  Copyright (c) 2015 Graciela Lucena. All rights reserved.
//

#import "MTLModel.h"
#import <Foundation/Foundation.h>
#import <Mantle.h>

@interface User : MTLModel

@property (copy, nonatomic) NSString *userId;
@property (copy, nonatomic) NSString *name;
@property (copy, nonatomic) NSString *usename;
@property (copy, nonatomic) NSURL *userAvatar;

@end
