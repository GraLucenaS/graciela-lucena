//
//  PopularShotsViewController.m
//  iOSTest
//
//  Created by Graciela Lucena on 9/28/15.
//  Copyright (c) 2015 Graciela Lucena. All rights reserved.
//

#import "PopularShotsViewController.h"
#import "dribbbleConnection.h"
#import "ShotTableViewCell.h"
#import "Shots.h"
#import "DetailViewController.h"

#import "UIScrollView+InfiniteScroll.h"

@interface PopularShotsViewController ()

@property (strong, nonatomic) NSMutableArray *shots;
@property (strong, nonatomic) Shots *selectedShot;
@property (strong, nonatomic) UIRefreshControl *refreshControl;
@property (strong, nonatomic) NSMutableArray *shotsData;

@end

@implementation PopularShotsViewController

int pageToRetrieve = 1;

- (void)viewDidLoad {
    [super viewDidLoad];
    
    __weak typeof(self) weakSelf = self;
    
    // Do any additional setup after loading the view.
    
    // enable auto-sizing cells on iOS 8
    if([self.tableView respondsToSelector:@selector(layoutMargins)]) {
        self.tableView.rowHeight = UITableViewAutomaticDimension;
    }
    
    // Set custom indicator margin
    self.tableView.infiniteScrollIndicatorMargin = 40;
    
    // Add infinite scroll handler
    [self.tableView addInfiniteScrollWithHandler:^(UITableView *tableView) {
        [weakSelf getPopularShotsFromPage:^{
            // Finish infinite scroll animations
            [self.tableView finishInfiniteScroll];
        }];
    }];
    
    // Load initial data
    [self getPopularShotsFromPage:nil];
    
}

- (void)viewWillAppear:(BOOL)animated{
    [super viewWillAppear:animated];
    [self.navigationBar setTitle:@"Dribbble Popular Shots"];
    [self.navigationController.navigationBar
     setTitleTextAttributes:@{NSForegroundColorAttributeName : [UIColor whiteColor]}];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

-(void) getPopularShotsFromPage:(void(^)(void))completion{
    [[dribbbleConnection sharedInstance] fetchPopularShotsFromPage:pageToRetrieve onCompletion:^(NSMutableArray *shots, NSError *error){
        if (!error) {
            if (pageToRetrieve != 1) {
                self.shotsData = shots;
                for (Shots *shotNew in self.shotsData) {
                    [self.shots addObject:shotNew];
                }
            }else{
                self.shots = shots;
            }
            [self.tableView reloadData];
        }
        pageToRetrieve += 1;
        
        if(completion) {
            completion();
        }

        
    }];
}

#pragma mark - UITableViewDataSource

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView {
    return 1;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    return self.shots.count;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    
    ShotTableViewCell *cell = (ShotTableViewCell*)[tableView dequeueReusableCellWithIdentifier:@"shotCell"];
    cell.PopularShot = [self.shots objectAtIndex:indexPath.row];
    [cell setUpCell];
    return cell;
}

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
{
    if ([[UIScreen mainScreen] bounds].size.height == 1024) return 530;
    
    return 280;
}

#pragma mark - UITableViewDelegate

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath{
    
    self.selectedShot = [self.shots objectAtIndex:indexPath.row];
    [self performSegueWithIdentifier:@"detailSegue" sender:nil];
}


#pragma mark - Navigation
-(void) prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender{
    
    if ([segue.identifier isEqualToString:@"detailSegue"]) {
        DetailViewController *shotDetailView = (DetailViewController*) segue.destinationViewController;
        shotDetailView.selectedShot = self.selectedShot;
    }
}


@end
