//
//  ShotTableViewCell.h
//  iOSTest
//
//  Created by Graciela Lucena on 9/29/15.
//  Copyright (c) 2015 Graciela Lucena. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "Shots.h"

@interface ShotTableViewCell : UITableViewCell

@property (weak, nonatomic) IBOutlet UIImageView *shotImage;
@property (weak, nonatomic) IBOutlet UILabel *shotTittle;
@property (weak, nonatomic) IBOutlet UILabel *shotViews;
@property (strong, nonatomic) Shots *PopularShot;

-(void)setUpCell;
@end
