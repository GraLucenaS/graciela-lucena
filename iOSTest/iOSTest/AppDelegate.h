//
//  AppDelegate.h
//  iOSTest
//
//  Created by Graciela Lucena on 9/30/15.
//  Copyright (c) 2015 Graciela Lucena. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface AppDelegate : UIResponder <UIApplicationDelegate>

@property (strong, nonatomic) UIWindow *window;


@end

