//
//  Shots.h
//  iOSTest
//
//  Created by Graciela Lucena on 9/28/15.
//  Copyright (c) 2015 Graciela Lucena. All rights reserved.
//

#import "MTLModel.h"
#import <Foundation/Foundation.h>
#import <Mantle.h>

@class User;

@interface Shots : MTLModel

@property (strong, nonatomic) NSNumber *shotId;
@property (strong, nonatomic) NSString *shotTittle;
@property (strong, nonatomic) NSNumber *shotViews;
@property (strong, nonatomic) NSURL *imageUrl;
@property (copy, nonatomic) NSString *shotDescription;
@property (strong, nonatomic) User *user;

@end
