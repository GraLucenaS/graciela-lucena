//
//  User.m
//  iOSTest
//
//  Created by Graciela Lucena on 9/28/15.
//  Copyright (c) 2015 Graciela Lucena. All rights reserved.
//

#import "User.h"

@interface User () <MTLJSONSerializing>

@end

@implementation User

#pragma mark - Mantle related methods
+ (NSDictionary *)JSONKeyPathsByPropertyKey {
    return @{
             @"userId"      : @"id",
             @"name": @"name",
             @"username": @"username",
             @"userAvatar": @"avatar_url"
             };
}

@end
