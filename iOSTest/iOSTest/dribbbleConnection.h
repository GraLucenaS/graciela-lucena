//
//  dribbbleConnection.h
//  iOSTest
//
//  Created by Graciela Lucena on 9/28/15.
//  Copyright (c) 2015 Graciela Lucena. All rights reserved.
//

#import "AFHTTPSessionManager.h"
#import <AFNetworking.h>

typedef void (^FetchShotsCompletionBlock)(NSMutableArray *notes, NSError *error);

@interface dribbbleConnection : AFHTTPSessionManager

+ (instancetype)sharedInstance;

- (void)fetchPopularShotsFromPage:(int)page onCompletion:(FetchShotsCompletionBlock)completionBlock;

@end
