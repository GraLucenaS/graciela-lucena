//
//  DetailViewController.m
//  iOSTest
//
//  Created by Graciela Lucena on 9/29/15.
//  Copyright (c) 2015 Graciela Lucena. All rights reserved.
//

#import "DetailViewController.h"
#import "Shots.h"
#import <SDWebImage/UIImageView+WebCache.h>
#import "User.h"

@interface DetailViewController ()

@end

@implementation DetailViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    [self loadInformation];
    // Do any additional setup after loading the view.
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (void)loadInformation{
    
    [self.navigationItem setTitle:@"Shot Detail"];
    [self.navigationController.navigationBar setTintColor:[UIColor whiteColor]];
    [self.navigationController.navigationBar
     setTitleTextAttributes:@{NSForegroundColorAttributeName : [UIColor whiteColor]}];
    
    [self.shotImage sd_setImageWithURL:self.selectedShot.imageUrl];
    [self.shotTittle setText:self.selectedShot.shotTittle];
    [self.viewsCount setText:[self.selectedShot.shotViews stringValue]];
    [self.shotDescription setText:self.selectedShot.shotDescription];
    [self.userAvatar sd_setImageWithURL:self.selectedShot.user.userAvatar];
    [self.userName setText:self.selectedShot.user.name];
    self.userAvatar.clipsToBounds = YES;
    [self.userAvatar.layer setCornerRadius:self.userAvatar.frame.size.width/2];

}

- (IBAction)share:(id)sender {
    
    NSString *text = @"Popular Photo in Dribbble";
    NSURL *url = self.selectedShot.imageUrl;
    
    UIActivityViewController *controller = [[UIActivityViewController alloc]initWithActivityItems:@[text, url]applicationActivities:nil];
    
    controller.excludedActivityTypes = @[UIActivityTypePostToWeibo,
                                         UIActivityTypeMessage,
                                         UIActivityTypeMail,
                                         UIActivityTypePrint,
                                         UIActivityTypeCopyToPasteboard,
                                         UIActivityTypeAssignToContact,
                                         UIActivityTypeSaveToCameraRoll,
                                         UIActivityTypeAddToReadingList,
                                         UIActivityTypePostToFlickr,
                                         UIActivityTypePostToVimeo,
                                         UIActivityTypePostToTencentWeibo,
                                         UIActivityTypeAirDrop];
    
    [self presentViewController:controller animated:YES completion:nil];
}


/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
