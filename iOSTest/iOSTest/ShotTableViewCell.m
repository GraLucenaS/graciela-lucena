//
//  ShotTableViewCell.m
//  iOSTest
//
//  Created by Graciela Lucena on 9/29/15.
//  Copyright (c) 2015 Graciela Lucena. All rights reserved.
//

#import "ShotTableViewCell.h"
#import <SDWebImage/UIImageView+WebCache.h>

@implementation ShotTableViewCell

-(void)setUpCell{
    
    [self.shotTittle setText:self.PopularShot.shotTittle];
    [self.shotViews setText:[self.PopularShot.shotViews stringValue]];
    SDWebImageManager *manager = [SDWebImageManager sharedManager];
    [manager downloadImageWithURL:self.PopularShot.imageUrl
                          options:0
                         progress:^(NSInteger receivedSize, NSInteger expectedSize) {
                         
                         }
                        completed:^(UIImage *image, NSError *error, SDImageCacheType cacheType, BOOL finished, NSURL *imageURL) {
                            if (image) {
                                [self.shotImage setImage:image];
                            }
                        }];
}

@end
