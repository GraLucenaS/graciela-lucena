//
//  dribbbleConnection.m
//  iOSTest
//
//  Created by Graciela Lucena on 9/28/15.
//  Copyright (c) 2015 Graciela Lucena. All rights reserved.
//

#import "dribbbleConnection.h"
#import <AFNetworkActivityIndicatorManager.h>
#import "Shots.h"

static NSString * const dribbbleBaseUrl = @"https://api.dribbble.com/";
static NSString *accessToken = @"bbfbdd279e35a44a610e43c3910109b5c711dfef60d76981d16ff9e1fb39d849";

@implementation dribbbleConnection

+ (instancetype)sharedInstance {
    static dribbbleConnection *_sharedInstance = nil;
    static dispatch_once_t onceToken;
    dispatch_once(&onceToken, ^{
        // Network activity indicator manager setup
        [[AFNetworkActivityIndicatorManager sharedManager] setEnabled:YES];
        
        // Session configuration setup
        NSURLSessionConfiguration *sessionConfiguration = [NSURLSessionConfiguration defaultSessionConfiguration];
        NSURLCache *cache = [[NSURLCache alloc] initWithMemoryCapacity:10 * 1024 * 1024     // 10MB. memory cache
                                                          diskCapacity:50 * 1024 * 1024     // 50MB. on disk cache
                                                              diskPath:nil];
        
        sessionConfiguration.URLCache = cache;
        sessionConfiguration.requestCachePolicy = NSURLRequestUseProtocolCachePolicy;
        
        // Initialize the session
        _sharedInstance = [[dribbbleConnection alloc] initWithBaseURL:[NSURL URLWithString:dribbbleBaseUrl] sessionConfiguration:sessionConfiguration];
    });
    
    return _sharedInstance;
}

- (instancetype)initWithBaseURL:(NSURL *)url sessionConfiguration:(NSURLSessionConfiguration *)configuration
{
    self = [super initWithBaseURL:url sessionConfiguration:configuration];
    if (!self) return nil;
        
    // Reachability setup
    [self.reachabilityManager startMonitoring];
    
    return self;
}

#pragma mark - Popular Shots fetching
- (void)fetchPopularShotsFromPage:(int)page onCompletion:(FetchShotsCompletionBlock) completionBlock
{
    NSString *path = [NSString stringWithFormat:@"shots/popular?page=%d", page];
    [self GET:path parameters:nil success:^(NSURLSessionDataTask *task, id responseObject) {
        NSMutableArray *shots = [[NSMutableArray alloc] initWithCapacity:[responseObject[@"shots"] count]];
        for (NSDictionary *JSONShotData in responseObject[@"shots"]) {
            Shots *shot = [MTLJSONAdapter modelOfClass:[Shots class] fromJSONDictionary:JSONShotData error:nil];
            
        if (shot) [shots addObject:shot];
        }
        
        completionBlock(shots, nil);
    }
      failure:^(NSURLSessionDataTask *task, NSError *error) {
        completionBlock(nil, error);
    }];
}

@end
