//
//  PopularShotsViewController.h
//  iOSTest
//
//  Created by Graciela Lucena on 9/28/15.
//  Copyright (c) 2015 Graciela Lucena. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface PopularShotsViewController : UIViewController <UITableViewDelegate, UITableViewDataSource>

@property (weak, nonatomic) IBOutlet UITableView *tableView;
@property (weak, nonatomic) IBOutlet UINavigationItem *navigationBar;


@end
